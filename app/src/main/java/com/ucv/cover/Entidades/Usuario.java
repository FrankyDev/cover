package com.ucv.cover.Entidades;

public class Usuario {
    private String Nombre;
    private String Sexo;
    private String Edad;
    private String Telefono;

    public Usuario() {
    }

    public Usuario(String nombre, String sexo, String edad, String telefono) {
        Nombre = nombre;
        Sexo = sexo;
        Edad = edad;
        Telefono = telefono;
    }

    //TODO encapsulacion
    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getSexo() {
        return Sexo;
    }

    public void setSexo(String sexo) {
        Sexo = sexo;
    }

    public String getEdad() {
        return Edad;
    }

    public void setEdad(String edad) {
        Edad = edad;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String telefono) {
        Telefono = telefono;
    }
}
