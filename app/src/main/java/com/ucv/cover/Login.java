package com.ucv.cover;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.Toast;

import com.hitomi.cmlibrary.CircleMenu;
import com.hitomi.cmlibrary.OnMenuSelectedListener;

public class Login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        CircleMenu circleMenu=findViewById(R.id.circle_menu);
        /*final String[] menus ={
                "facebook","google","correo","telefono"
        };*/

        circleMenu.setMainMenu(Color.parseColor("#F3B705"),R.drawable.inicio,R.drawable.cerrar)
                .addSubMenu(Color.parseColor("#4267B2"),R.drawable.facebook)
                .addSubMenu(Color.parseColor("#E44034"),R.drawable.google)
                .addSubMenu(Color.parseColor("#4082EE"),R.drawable.correo)
                .addSubMenu(Color.parseColor("#4CD142"),R.drawable.telefono)
                .setOnMenuSelectedListener(new OnMenuSelectedListener() {
                    @Override
                    public void onMenuSelected(int i) {
                        //Toast.makeText(Login.this,"Tu elegiste: "+menus[i],Toast.LENGTH_SHORT).show();
                        switch (i){
                            case 0: startActivity(new Intent(Login.this,LoginCorreo.class));break;
                            case 1: startActivity(new Intent(Login.this,Validacion.class));break;
                        }

                    }
                });



    }
}
