package com.ucv.cover.Entidades;

public class Nota {

    //TODO atributos
    private String Titulo;
    private String Contenido;

    //TODO Constructor
    public Nota() {
    }

    public Nota(String titulo, String contenido) {
        Titulo = titulo;
        Contenido = contenido;
    }

    //TODO encapsulacion
    public String getTitulo() {
        return Titulo;
    }

    public void setTitulo(String titulo) {
        Titulo = titulo;
    }

    public String getContenido() {
        return Contenido;
    }

    public void setContenido(String contenido) {
        Contenido = contenido;
    }
}
